package sampleconsts

import "os"

const (
	SampleConst    = 1
	AChar          = 'A'
	ZChar          = 'Z'
	PathSeparator  = string(os.PathSeparator)
	NewLineMac     = "\n"
	NewLineUnix    = "\n"
	NewLineWindows = "\r\n"
)

package mutexeg

import (
	"fmt"
	"sync"
)

type ParallelMutex struct {
	sync.Mutex
	stringsCollection *[]*string
}

func NewParallelMutex(capacity int) *ParallelMutex {
	// var arr []*string -> gives you an array
	collection := make([]*string, 0, capacity)

	return &ParallelMutex{
		Mutex:             sync.Mutex{}, // it will be created if not defined as well
		stringsCollection: &collection,
	}
}

func (parallelMutex *ParallelMutex) Add(str *string) {
	*parallelMutex.stringsCollection = append(
		*parallelMutex.stringsCollection,
		str)
}

func (parallelMutex *ParallelMutex) adds(stringElements *[]*string) {
	*parallelMutex.stringsCollection = append(
		*parallelMutex.stringsCollection,
		*stringElements...)
}

func (parallelMutex *ParallelMutex) Adds(stringElements ...*string) {
	if stringElements == nil {
		return
	}

	parallelMutex.adds(&stringElements)
}

func (parallelMutex *ParallelMutex) AddWithLock(str *string) {
	parallelMutex.Lock()
	defer parallelMutex.Unlock()

	parallelMutex.Add(str)
}

func (parallelMutex *ParallelMutex) AddsWithLock(stringElements ...*string) {
	if stringElements == nil {
		return
	}

	parallelMutex.Lock()
	defer parallelMutex.Unlock()

	parallelMutex.adds(&stringElements)
}

func (parallelMutex *ParallelMutex) IsEmpty() bool {
	return parallelMutex.stringsCollection == nil ||
		*parallelMutex.stringsCollection == nil ||
		len(*parallelMutex.stringsCollection) == 0
}

func (parallelMutex *ParallelMutex) Collection() []string {
	results := make([]string, len(*parallelMutex.stringsCollection))

	for i, strPtr := range *parallelMutex.stringsCollection {
		results[i] = *strPtr
	}

	return results
}

func (parallelMutex *ParallelMutex) String() string {
	isEmptyStr := fmt.Sprintf("IsEmpty : %+v", parallelMutex.IsEmpty())
	collection := fmt.Sprintf(",\nCollection : %+v\n", parallelMutex.Collection())

	return isEmptyStr + collection
}

func (parallelMutex *ParallelMutex) Print() {
	fmt.Println(parallelMutex)
}

func (parallelMutex *ParallelMutex) PrintLock() {
	parallelMutex.Lock()
	defer parallelMutex.Unlock()

	fmt.Println(parallelMutex)
}

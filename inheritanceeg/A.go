package inheritanceeg

import (
	"encoding/json"
	"fmt"
)

type A struct {
	Name             string
	AnotherBaseField string
}

// Should be moved to B.go
type B struct {
	A
	Address     string
	Name        string
	privateName string
}

// This recommended when you have multiple things in one pkg
func NewB(a A, address string) B {
	return B{
		A:       a,
		Address: address,
		Name:    a.Name + "- B's Name",
	}
}

func (b *B) String() string {
	return fmt.Sprintf("%#v", b)
}

// Will not work, because to update you need attach with pointer object.
func (b B) SetName(name string) {
	b.Name = name
}

func (b B) MarshalJSON() ([]byte, error) {
	fmt.Println("MarshalJSON to A, overriding")

	return json.Marshal(b.A)
}

func (b *B) UnmarshalJSON(data []byte) error {
	fmt.Println("UnmarshalJSON, overriding")

	return json.Unmarshal(data, b.A)
}

// Will not work, because to update you need attach with pointer object.
func (b B) SetPrivateName(name string) {
	b.privateName = name
}

// not GetPrivateName
func (b B) PrivateName() string {
	return b.Name
}

func (b *B) Json() string {
	allBytes, err := json.Marshal(b)

	if err != nil {
		// handle it gracefully
		// 1. Can be return back with the result
		// 2. Or, last resort panic

		panic(err)

		// Why panic is bad:
		// It slows down, user needs to recover from it, which is expensive.
		// It only better to panic at the last stage of the application.
		// For example, pkg func1 call if that throws and
		// you have pkg2 call that and you don't panic here but at the application UI level then
		// you have to recover and send back to application UI level

		// Worst !important:
		// What is more bad is just checking no error and proceed.
	}

	return string(allBytes)
}

func GetB(jsonStr string) B {
	var b B
	json.Unmarshal([]byte(jsonStr), &b)

	return b
}

//
// func GetB(jsonStr string) B {
// 	var b B
// 	json.Unmarshal([]byte(jsonStr), &b.A)
//
// 	return b
// }
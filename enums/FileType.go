package enums

type FileType byte

var (
	// remember to have the , at the end.
	fileTypes = []string{
		"Jpg", "Exe", "Bmp", "Pdf", "unExport", "Bat",
	}
)

const (
	Jpg FileType = iota
	Exe
	Bmp
	Pdf
	unExport
	Bat
)

func (fileType FileType) Is(fileT FileType) bool {
	return fileType == fileT
}

func (fileType FileType) IsJpg() bool {
	return fileType == Jpg
}

func (fileType FileType) Byte() byte {
	return byte(fileType)
}

func (fileType FileType) String() string {
	return fileTypes[fileType]
}

package enums

type DirType byte

var (
	// remember to have the , at the end.
	dirTypes = []string{
		"System", "Documents", "Downloads", "Gaming",
	}
)

const (
	System DirType = iota
	Documents
	Downloads
	Gaming
)

// const (
// 	Jpg DirType = iota
// 	Exe
// 	Bmp
// 	Pdf
// 	unExport
// 	Bat
// )

func (d DirType) Is(d2 DirType) bool {
	return d == d2
}

func (d DirType) IsSystem() bool {
	return d == System
}

func (d DirType) Byte() byte {
	return byte(d)
}

func (d DirType) String() string {
	return fileTypes[d]
}

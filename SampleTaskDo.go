package sampletask

func SampleTaskDo() {
	panic("not implemented")
}

func Do() {
	panic("not implemented, better name than SampleTaskDo. You can call it, sampletask.do")
}
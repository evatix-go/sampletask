package ca

type A struct {
	data string
}

func (a *A) DoCA() string {
	return "DoCA()"
}

func (a *A) DoCA2() string {
	return "DoCA2()"
}

package cab

import (
	"gitlab.com/evatix-go/sampletask/cyclefix/ca"
	"gitlab.com/evatix-go/sampletask/cyclefix/cb"
)

type AB struct {
	Data string
}

func (ab *AB) DoCA() string {
	a := ca.A{}

	return a.DoCA()
}

func (ab *AB) DoCB() string {
	b := cb.B{}

	return b.DoCB()
}

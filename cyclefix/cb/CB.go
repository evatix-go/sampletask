package cb

type B struct {
	Data string
}

func (b *B) DoCB() string {
	return "DoCB()"
}

func (b *B) DoCB2() string {
	return "DoCB2()"
}

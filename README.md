# `sampletask` gopackage Example

- a logo is kind of must.

![Use Package logo](UseLogo)

## Git Clone

`git clone gitlab.com/evatix-go/sampletask.git`

### 2FA enabled, for linux

`git clone https://[YourGitLabUserName]:[YourGitlabAcessTokenGenerateFromGitlabsTokens]@gitlab.com/evatix-go/sampletask.git`

### Prerequisites

- Update git to latest 2.29
- Update or install the latest of Go 1.15.2
- Either add your ssh key to your gitlab account
- Or, use your access token to clone it.

## Installation

`go get gitlab.com/evatix-go/sampletask`

## Binary Installation

`go get -u gitlab.com/evatix-go/sampletask`

### Go get issue for private package

- Update git to 2.29
- Enable go modules. (Windows : `go env -w GO111MODULE=on`, Unix : `export GO111MODULE=on`)
- Add `gitlab.com/evatix-go` to go env private
  
To set for Windows:

`go env -w GOPRIVATE=[AddExistingOnes;]gitlab.com/evatix-go`

To set for Unix:

`expoort GOPRIVATE=[AddExistingOnes;]gitlab.com/evatix-go`

## Why `sampletask?`

## Examples

`Code Smaples`

## Acknowledgement

Any other packages used

## Links

* [dmarkham/enumer: A Go tool to auto generate methods for your enums](https://github.com/dmarkham/enumer)
* [inheritance - Golang Method Override - Stack Overflow](https://stackoverflow.com/questions/38123911/golang-method-override)

## Issues

- [Create your issues](https://gitlab.com/evatix-go/sampletask/-/issues)

## Notes

## Contributors

- [CTO of CIMUX, Md. Alim Ul Karim](http://google.com?q=Alim-Ul-Karim) | [Alim Ul Karim Linkedin](https://www.linkedin.com/in/alimkarim)

## License

[Evatix MIT License](/LICENSE)

package filetype

import "fmt"

type Variant byte

var (
	// remember to have the , at the end.
	fileTypes = []string{
		"Jpg", "Exe", "Bmp", "Pdf", "unExport", "Bat",
	}
)

const (
	Jpg Variant = iota
	Exe
	Bmp
	Pdf
	unExport
	Bat
)

func (v Variant) Is(fileT Variant) bool {
	return v == fileT
}

// Wrong to have diff naming.
func (variant Variant) IsJpg() bool {
	return variant == Jpg
}

func (v Variant) Value() byte {
	return byte(v)
}

func (v Variant) Byte() byte {
	return byte(v)
}

func (v Variant) String() string {
	return fileTypes[v]
}

func (v Variant) Panic(msg interface{}) string {
	panic(fmt.Sprintf("%s %s", v.String(), msg))
}

func RangesStrings() []string {
	return fileTypes
}

func Max() byte {
	return Jpg.Byte()
}

func Min() byte {
	return Bat.Byte()
}

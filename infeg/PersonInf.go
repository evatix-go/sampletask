package infeg

// writer, -er at the end
type Personer interface {
	Namer
	Addresser
	TaskDoer
}

type Namer interface {
	Name() string
}

type Addresser interface {
	Address() string
}

type TaskDoer interface {
	TaskDo() string
}

func DoThingsWithName(namer Namer) string{
	return namer.Name()
}

func DoThingsWithAddress(addresser Addresser) string{
	return addresser.Address()
}
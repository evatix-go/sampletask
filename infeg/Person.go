package infeg

type Person struct {
	name, address string
}

func (p *Person) TaskDo() string {
	return "TaskDo"
}

func (p *Person) Name() string {
	return p.name
}

func (p *Person) Address() string {
	return p.address
}

func (p *Person) ToGeneric() Personer {
	return p
}

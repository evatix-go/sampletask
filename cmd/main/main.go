package main

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/evatix-go/sampletask/cyclefix/cab"
	"gitlab.com/evatix-go/sampletask/inheritanceeg"
	"gitlab.com/evatix-go/sampletask/mutexeg"
)

func workWithCycle() {
	ab := cab.AB{Data: "hello"}

	fmt.Println(ab.DoCA())
	fmt.Println(ab.DoCB())
}

func workWithParallelMutexSimple() {
	parallelMutex := mutexeg.NewParallelMutex(20)

	for i := 0; i < 5000; i++ {
		newElem := string(i) + "cur"
		parallelMutex.Adds(&newElem)
	}

	parallelMutex.Print()
}

func workWithParallelMutexSimpleFixBytes() {
	parallelMutex := mutexeg.NewParallelMutex(20)

	for i := 0; i < 5000; i++ {
		newElem := fmt.Sprintf("%d . ", i) + "cur"
		parallelMutex.Adds(&newElem)
	}

	parallelMutex.Print()
}

func workWithParallelMutexSimpleFixBytesAsync() {
	parallelMutex := mutexeg.NewParallelMutex(20)

	curFunc := func(i int) {
		newElem := fmt.Sprintf("%d . ", i) + "cur"
		parallelMutex.AddWithLock(&newElem)
		duration := time.Duration(rand.Intn(200 + i)) * time.Millisecond

		time.Sleep(duration)
	}

	for i := 0; i < 5000; i++ {
		go curFunc(i)
	}

	parallelMutex.PrintLock()
}

func workWithB() {
	b := inheritanceeg.B{
		A: inheritanceeg.A{
			Name:             "A's Name",
			AnotherBaseField: "Another field",
		},
		Address: "Address",
		Name:    "Name self",
	}

	fmt.Println("b string:", b.String())

	json := b.Json()
	fmt.Println("b json:", b.Json())
	b2 := inheritanceeg.GetB(json)

	fmt.Println("b2 from b's JSON:", b2)

	b3 := inheritanceeg.NewB(b.A, b.Address)

	b3.SetName("Set current name")
	b3.SetPrivateName("Set current private name")

	fmt.Println("b3 :\n", b3.String())
}

func main() {
	// fmt.Println(
	// 	"Sample task build :",
	// 	sampletask.GetSampleString(),
	// 	sampletask.OsString(),
	// )

	// workWithB()

	// workWithCycle()
	// workWithParallelMutexSimple()
	// workWithParallelMutexSimpleFixBytes()
	workWithParallelMutexSimpleFixBytesAsync()
	// fmt.Println(filetype.RangesStrings())

	// fmt.Println(sampletask.GetEnumUseCase(enums.))
	// fmt.Println(filetype.RangesStrings())
	// filetype.Jpg.Panic("do a panic on jpeg files")
	// sampletask.SampleTaskDo()
	// sampletask.Do()
}

package scheduler

import "gitlab.com/evatix-go/sampletask/filetype"

type Scheduler struct {
	TaskName string
	FileType filetype.Variant
}

// NOT recommended in the named pkg
func NewScheduler (name string, filetype filetype.Variant) Scheduler {
	return Scheduler{
		TaskName: name,
		FileType: filetype,
	}
}

// Recommended
func New(name string, filetype filetype.Variant) Scheduler {
	return Scheduler{
		TaskName: name,
		FileType: filetype,
	}
}

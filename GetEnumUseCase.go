package sampletask

import "gitlab.com/evatix-go/sampletask/enums"

// You can write method documents like this : https://bit.ly/3nQbmWr
//  Selecting file for each case can give different result.
//      - 1 : JPG
//      - 2 : Exe https://play.golang.org/p/HmnNoBf0p1z
func GetEnumUseCase(fileType enums.FileType) string {
	return fileType.String()
}


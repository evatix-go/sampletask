package sampletask

import "gitlab.com/evatix-go/sampletask/filetype"

func GetFileTypeName(variant filetype.Variant) string {
	return variant.String()
}